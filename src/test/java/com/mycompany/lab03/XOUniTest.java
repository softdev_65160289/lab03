/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author krnis
 */
public class XOUniTest {
    
    public XOUniTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWin_O_horizontal_row1_output_true() { //check O row 1
        String table[][] = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_horizontal_row2_output_true() { //check O row 2
        String table[][] = {{"-", "-", "-"}, {"O", "O", "O"}, {"-", "-", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_horizontal_row3_output_true() { //check O row 3
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"O", "O", "O"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_horizontal_row1_output_true() { //check X row 1
        String table[][] = {{"X", "X", "X"}, {"-", "-", "-"}, {"-", "-", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_horizontal_row2_output_true() { //check X row 2
        String table[][] = {{"-", "-", "-"}, {"X", "X", "X"}, {"-", "-", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_horizontal_row3_output_true() { //check X row 3
        String table[][] = {{"-", "-", "-"}, {"-", "-", "-"}, {"X", "X", "X"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_vertical_col1_output_true() { //check O col 1
        String table[][] = {{"O", "-", "-"}, {"O", "-", "-"}, {"O", "-", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_vertical_col2_output_true() { //check O col 2
        String table[][] = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_vertical_col3_output_true() { //check O col 3
        String table[][] = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_vertical_col1_output_true() { //check X col 1
        String table[][] = {{"X", "-", "-"}, {"X", "-", "-"}, {"X", "-", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_vertical_col2_output_true() { //check X col 2
        String table[][] = {{"-", "X", "-"}, {"-", "X", "-"}, {"-", "X", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_vertical_col3_output_true() { //check X col 3
        String table[][] = {{"-", "-", "X"}, {"-", "-", "X"}, {"-", "-", "X"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Diagonal_Right_output_true() { //check O Diagonal Right
        String table[][] = {{"O", "-", "-"}, {"-", "O", "-"}, {"-", "-", "O"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Diagonal_Rigth_output_true() { //check X Diagonal Right
        String table[][] = {{"X", "-", "-"}, {"-", "X", "-"}, {"-", "-", "X"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_O_Diagonal_Left_output_true() { //check O Diagonal Left
        String table[][] = {{"-", "-", "O"}, {"-", "O", "-"}, {"O", "-", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckWin_X_Diagonal_Left_output_true() { //check X Diagonal Left
        String table[][] = {{"-", "-", "X"}, {"-", "X", "-"}, {"X", "-", "-"}};
        boolean result = Lab03.checkWin(table);
        assertEquals(true, result);
    }
    
    @Test
    public void testCheckDraw_output_true() { //check Draw
        String table[][] = {{"X", "O", "X"}, {"X", "O", "X"}, {"O", "-", "O"}};
        boolean result = Lab03.checkDraw(table);
        assertEquals(true, result);
    }
}